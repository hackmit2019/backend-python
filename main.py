from flask import Flask, jsonify, request
from analyze import process_one, process_all

app = Flask(__name__)


@app.route("/")
def hello_world():
    return "Hello, World!"


@app.route("/analyzeone", methods=["POST"])
def analyzeone():
    json1 = request.get_json()
    data = process_one(json1)
    print(data)
    return jsonify(data)

@app.route("/analyzeall", methods=["POST"])
def analyzeall():
    json1 = request.get_json()
    data = process_all(json1)
    print(data)
    return jsonify(data)
