import nltk
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.stem.porter import PorterStemmer
import requests
from nltk.sentiment.vader import SentimentIntensityAnalyzer

import spacy
from spacy import displacy
from collections import Counter
from rake_nltk import Rake


# python -m spacy download en_core_web_sm
nlp = spacy.load("en_core_web_sm")

sid = SentimentIntensityAnalyzer()


def concat_elements(part):
    s = ""
    for element in part["elements"]:
        if element["type"] in ["text", "punct"]:
            s += element["value"]

    return s


def get_ents(s):
    return list(map(str, nlp(s).ents))


def process_one(part):
    s = concat_elements(part)
    ents = get_ents(s)
    part_data = {}
    for e in ents:
        part_data[e] = []
        d = extract_wiki_data(e)
        if d:
            part_data[e].append(d)

    return {"highlight": ents, "extraData": part_data, "ss": sid.polarity_scores(s)}


def process_all(parts):
    r = Rake()
    text = ""
    for part in parts:
        text += concat_elements(part)

    r.extract_keywords_from_text(text)
    return {
        'keywords': r.get_ranked_phrases_with_scores()
    }


def extract_wiki_data(name):
    res = requests.get(
        f"https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&titles={name}"
    )
    json = res.json()
    pageid = list(json["query"]["pages"].keys())[0]
    try:
        extract = json["query"]["pages"][pageid]["extract"][:200] + '...'
    except KeyError:
        return None

    return {
        "text": extract,
        "link": f"https://en.wikipedia.org/?curid={pageid}",
        "source": "wiki",
    }
